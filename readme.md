# Blogger

Assignment for App Development. A blogging website that allows user to set up and maintain multiple blogs.

[Deployed to Heroku] (https://rshelly-assignment2.herokuapp.com/)

## Features:

* Create account
* Create maintain blogs
* Create static webpages
* Comment on blog posts
* Attach pictures/videos to blog posts
* Customise blog layout and colour scheme

## Languages

* Java
* Javascript
* HTML



## Frameworks

* Play 1.2.7 Framework
* Semantic UI
