import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;
import models.Blog;
import models.Page;
import models.User;


public class PageTest extends UnitTest{
  
  private User ringo;
  private Blog theBand;
  private Page members;
  private Page discography;
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setup() {
    ringo = new User("Ringo", "Starr", "Drummer for the Beatles", "www.thebeatles.com/ringo", "ringo@Starr.com", "rstarr", "secret");
    ringo.save();
    
    theBand = new Blog(ringo, "The Band", "BLog with all the latest news on The Beatles");
    theBand.save();
    
    members = new Page(theBand, "Members", "John, Paul, George and Ringo");
    members.save();    
    discography = new Page(theBand, "Discography", "Yellow Submarine, Abbey Road");
    discography.save();
    
    ringo.blogs.add(theBand);
    ringo.save();
    }
  
  @After
  public void teardown() {
    theBand.pages.clear();
    members.delete();
    discography.delete();
    ringo.blogs.clear();
    theBand.delete();
    ringo.delete();
  }
  
  @Test
  public void addPage() {
    ringo.blogs.get(0).pages.add(members);
    ringo.save();
    assertEquals(1, ringo.blogs.get(0).pages.size());
  }
  
  @Test
  public void addMultiplePages() {
    ringo.blogs.get(0).pages.add(members);
    ringo.blogs.get(0).pages.add(discography);
    ringo.save();
    assertEquals(2, ringo.blogs.get(0).pages.size());
  }
  
  @Test
  public void testPageContet() {
    ringo.blogs.get(0).pages.add(members);
    ringo.save();
    assertEquals(theBand, ringo.blogs.get(0).pages.get(0).blog);
    assertEquals("Members", ringo.blogs.get(0).pages.get(0).title);
    assertEquals("John, Paul, George and Ringo", ringo.blogs.get(0).pages.get(0).content);
  }
}
