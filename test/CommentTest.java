import models.Blog;
import models.Comment;
import models.Post;
import models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;


public class CommentTest extends UnitTest{
  
  private User john;
  private User paul;
  private User george;
  private Blog music;
  private Post newSong;
  private Comment firstComment;
  private Comment secondComment;
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setup() {
    john = new User("John", "Lennon", "Vocalist and rythm guitarist for The Beatles", "www.thebeatles.com/john", "john@lennon.com", "jlennon", "secret");
    john.save();
    paul = new User("Paul", "McCartney", "Vocalist and bass guitarist for The Beatles", "www.thebeatles.com/paul", "paul@mccartney.com", "pmccarntey", "secret");
    paul.save();
    george = new User("George", "Harrison", "Lead guitarist in The Beatles", "www.thebeatles.com/george", "george@harrison.com", "gharrison", "secret");
    george.save();
    music = new Blog(john, "Music", "Blog about music I write");
    music.save();
    newSong = new Post(music, "New Song", "I'm working on a new song for our next album", "", "");
    newSong.save();
    firstComment = new Comment(newSong, paul, "Nice work");
    firstComment.save();
    secondComment = new Comment(newSong, george, "Thought of a name for it?");
    secondComment.save();
    
    john.blogs.add(music);
    john.blogs.get(0).posts.add(newSong);
    john.save();
  }
  
  @After
  public void teardown() {
    newSong.comments.clear();
    music.posts.clear();
    john.blogs.clear();
    firstComment.delete();
    secondComment.delete();
    newSong.delete();
    music.delete();
    john.delete();
    paul.delete();
    george.delete();
  }
  
  @Test
  public void addComment() {
    john.blogs.get(0).posts.get(0).comments.add(firstComment);
    assertEquals(1, john.blogs.get(0).posts.get(0).comments.size());
  }
  
  @Test
  public void addMultipleComments() {
    john.blogs.get(0).posts.get(0).comments.add(firstComment);
    john.blogs.get(0).posts.get(0).comments.add(secondComment);
    assertEquals(2, john.blogs.get(0).posts.get(0).comments.size());
  }
  
  @Test
  public void testCommentContent() {
    john.blogs.get(0).posts.get(0).comments.add(firstComment);
    assertEquals(paul, john.blogs.get(0).posts.get(0).comments.get(0).author);
    assertEquals(newSong, john.blogs.get(0).posts.get(0).comments.get(0).post);
    assertEquals("Nice work", john.blogs.get(0).posts.get(0).comments.get(0).content);
  }
}
