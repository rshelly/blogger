import models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;


public class UserTest extends UnitTest{
  
  private User john;
  private User paul;
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setup() {
    john = new User("John", "Lennon", "Vocalist and rythm guitarist for The Beatles", "www.thebeatles.com/john", "john@lennon.com", "jlennon", "secret");
    john.save();
    paul = new User("Paul", "McCartney", "Vocalist and bass guitarist for The Beatles", "www.thebeatles.com/paul", "paul@mccartney.com", "pmccarntey", "secret");
    paul.save();
  }
  
  @After
  public void teardown() {
    john.delete();
    paul.delete();    
  }
  
  @Test
  public void createUser() {
    User testUser = User.findByEmail("john@lennon.com");
    assertNotNull(testUser);
  }
  
  @Test
  public void createMultipleUsers() {
    User firstTestUser = User.findByEmail("john@lennon.com");
    User secondTestUser = User.findByEmail("paul@mccartney.com");
    assertNotNull(firstTestUser);
    assertNotNull(secondTestUser);
  }
  
  @Test
  public void testUserDetails() {
    assertEquals(john.firstName, "John");
    assertEquals(john.lastName,"Lennon");
    assertEquals(john.about, "Vocalist and rythm guitarist for The Beatles");
    assertEquals(john.avatarUrl, "www.thebeatles.com/john");
    assertEquals(john.email, "john@lennon.com");
    assertEquals(john.username, "jlennon");
    assertEquals(john.password, "secret");
  }
}
