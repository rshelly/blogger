import models.Blog;
import models.Post;
import models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;


public class PostTest extends UnitTest{
  
  private User ringo;
  private Blog albums;
  private Post abbeyRoad;
  private Post yellowSubmarine;
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setup() {
    ringo = new User("Ringo", "Starr", "Drummer for the Beatles", "www.thebeatles.com/ringo", "ringo@Starr.com", "rstarr", "secret");
    ringo.save();
    
    albums = new Blog(ringo, "Albums", "Blog about our albums");
    albums.save();
    
    abbeyRoad = new Post(albums, "Abbey Road", "Abbey Road is the eleventh studio album by the Beatles", "www.thebeatles.com/abbeyroad-albumcover", "www.thebeatles.com.abbeyroad-promovid");
    abbeyRoad.save();
    yellowSubmarine = new Post(albums, "Yellow Submarine", "Yellow Submarine is the tenth studio album by the Beatles", "www.thebeatles.com/yellowsubmarine-albumcover", "www.thebeatles.com.yellowsubmarine-promovid");
    yellowSubmarine.save();
    
    ringo.blogs.add(albums);
    ringo.save();
  }
  
  @After
  public void teardown() {
    albums.posts.clear();
    abbeyRoad.delete();
    yellowSubmarine.delete();
    ringo.blogs.clear();
    albums.delete();
    ringo.delete();    
  }
  
  @Test
  public void addPost() {
    ringo.blogs.get(0).posts.add(abbeyRoad);
    ringo.save();
    assertEquals(1, ringo.blogs.get(0).posts.size());
  }
  
  @Test
  public void addMultiplePosts() {
    ringo.blogs.get(0).posts.add(abbeyRoad);
    ringo.blogs.get(0).posts.add(yellowSubmarine);
    ringo.save();
    assertEquals(2, ringo.blogs.get(0).posts.size());
  }
  
  @Test
  public void testPostContent() {
    ringo.blogs.get(0).posts.add(abbeyRoad);
    ringo.save();
    assertEquals(albums, ringo.blogs.get(0).posts.get(0).blog);
    assertEquals("Abbey Road", ringo.blogs.get(0).posts.get(0).title);
    assertEquals("Abbey Road is the eleventh studio album by the Beatles",
        ringo.blogs.get(0).posts.get(0).content);
    assertEquals("www.thebeatles.com/abbeyroad-albumcover", ringo.blogs.get(0).posts.get(0).imageUrl);
    assertEquals("www.thebeatles.com.abbeyroad-promovid", ringo.blogs.get(0).posts.get(0).videoUrl);
  }
}
