import models.Blog;
import models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;


public class BlogTest extends UnitTest{
  
  private User george;
  private Blog music;
  private Blog lyrics;
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setup() {
    george = new User("George", "Harrison", "Lead guitarist in The Beatles", "www.thebeatles.com/george", "george@harrison.com", "gharrison", "secret");
    george.save();
    music = new Blog(george, "Music", "Blog about music I write");
    music.save();
    lyrics = new Blog(george, "Lyrics", "Blog about lyrics I write");
    lyrics.save();
  }
  
  @After
  public void teardown() {
    george.blogs.clear();
    music.delete();
    lyrics.delete();
    george.delete();
  }
  
  @Test
  public void addBlog() {
    george.blogs.add(music);
    george.save();
    assertEquals(1, george.blogs.size());
  }
  
  @Test
  public void addMultipleBlogs() {
    george.blogs.add(music);
    george.blogs.add(lyrics);
    assertEquals(2, george.blogs.size());
  }
  
  @Test
  public void testBlogsDetails() {
    george.blogs.add(music);
    assertEquals("George", george.blogs.get(0).blogger.firstName);
    assertEquals("Music", george.blogs.get(0).title);
    assertEquals("Blog about music I write", george.blogs.get(0).description);
  }
}
