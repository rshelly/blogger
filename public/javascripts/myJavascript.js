function showAddBlogForm(addToElement, removeFromElement, removeId) {
	var blogFormInnerHtml = ("<section id='hiddenSection' class='ui inverted segment'><form class='ui form' action='/newblog' method='POST'><div class='two fields'><div class='field'><input id='title' type='text' name='blog.title' placeholder='Title'></input></div><div class='field'><input id='description' type='text' name='blog.description' placeholder='Desription'></input></div></div><button class='ui button red submit mini labeled icon'><i class='icon edit'></i> Create </button></form></section>");
	showForm(addToElement, removeFromElement, removeId, blogFormInnerHtml);
}

function showAddPageForm(addToElement, removeFromElement, removeId) {
	var pageFormInnerHtml = ("<section id='hiddenSection' class='ui inverted segment'><div class='two fields'><div class='field'><input id='name' type='text' name='page.title' placeholder='Title'></input></div></div><div class='field'><textarea id='content' name='page.content' placeholder='Content'></textarea></div><div class='two fields'><div class='field'><input id='imageUrl' type='text' name='page.imageUrl' placeholder='Image URL'></input></div><div class='field'><input id='videoUrl' type='text' name='page.videoUrl' placeholder='Video URL'></input></div></div><button class='ui button red submit labeled icon'><i class='icon edit'></i> Create </button></section>");
	showForm(addToElement, removeFromElement, removeId, pageFormInnerHtml);
}


function showForm(addToElement, removeFromElement, removeId, innerHtml) {
	
	//Create div element then add Create Blog form to it
	var form = document.createElement('div');
	form.innerHTML = (innerHtml);
	
	//Get the element on the page that we want to add the form to
	var add = document.getElementById(addToElement);
	
	//Add form to the page
	add.appendChild(form);
	
	//Get the element on the page that we want to remove the "New Blog" button from
	var removeButton = document.getElementById(removeFromElement);
	  
	//Get the "New Blog" button
	var button = document.getElementById(removeId)
	 
	//Remove the New "Blog" from the page
	removeButton.removeChild(button)
}
	