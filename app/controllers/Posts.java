package controllers;

import models.Comment;
import models.User;
import models.Blog;
import models.Post;
import play.Logger;
import play.mvc.Controller;

public class Posts extends Controller {
  
  public static void listPosts(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));
    render(user, blog);
  }
  
  public static void viewPost(String postId) {
    User user = Accounts.getLoggedInUser();
    Post post = Post.findById(Long.parseLong(postId));
    render(user, post);
  }
  
  public static void createPost(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));
    render(user, blog);
  }
  
  public static void newPost(String blogId, String title, String content, String imageUrl, String videoUrl) {
    Blog blog = Blog.findById(Long.parseLong(blogId));
    
    Post post = new Post(blog, title, content, imageUrl, videoUrl);
    post.save();
    blog.posts.add(post);
    blog.save();
    
    listPosts("" + blog.id);
  } 
  
  public static void deletePost(String postId) {
    User user = Accounts.getLoggedInUser();
    Post post = Post.findById(Long.parseLong(postId));
    Blog blog = post.blog;
    
    int numComments = post.comments.size();
    blog.numComments -= numComments;
    
    post.delete();
    blog.save();
    user.save();
    
    listPosts("" + blog.id);
    
  }

}
