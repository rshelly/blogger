package controllers;

import models.Blog;
import models.Page;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class Settings extends Controller {
  
  public static void index(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));
    render(user, blog);
  }
  
  public static void setColour(String blogId, String colour) {
    Blog blog = Blog.findById(Long.parseLong(blogId));
    Logger.info("Changing colour of blog, " + blog.title + ", to " + colour);
    blog.colour = colour;
    blog.save();
    BlogView.index(blog.url);
  }
  
  public static void setMenuLayout(String blogId, String menuLayout) {
    Blog blog = Blog.findById(Long.parseLong(blogId));
    blog.menuLayout = menuLayout;
    blog.save();
    BlogView.index(blog.url);
  }
}
