package controllers;

import play.*;
import play.data.validation.Required;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller {

  public static void index() {
    render();
  }

  public static void signup() {
    render();
  }

  public static void login() {
    render();
  }
  
  public static void edit() {
    User user = Accounts.getLoggedInUser();
    render(user);
  }
  
  public static void logout() {
    session.clear();
    login();
  }

  public static void register(User user, String password2) {
//    if (password2.equals(user.password)) {
//      user.save();
//      login();
//    } else {
//      signup();
//    }
//    index();
    user.save();
    login();
  }

  public static void authenticate(String username, String password) {

    Logger.info("Attempting to authenticate with " + username + ":" + password);

    User user = User.findByUsername(username);
        
    if ((user != null) && (user.verifyPassword(password) == true)) {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      Home.index();
    } else {
      Logger.info("Authentication failed");
      login();
    }
  }
  
  public static User getLoggedInUser() {
    User user = null;
    if (session.contains("logged_in_userid")) {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    } else {
      login();
    }
    return user;
  }

}