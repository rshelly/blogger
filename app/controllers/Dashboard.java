package controllers;

import models.Blog;
import models.User;
import play.mvc.Controller;

public class Dashboard extends Controller {
  
  public static void index(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));    
    render(user, blog);
  }
  
  public static void deleteBlog(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));  
    
    blog.delete();
    user.save();
    Home.index();
  }

}
