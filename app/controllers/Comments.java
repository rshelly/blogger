package controllers;

import models.Blog;
import models.Comment;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class Comments extends Controller {

  public static void index(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));
    render(user, blog);
  }
  
  public static void deleteComment(String commentId) {
    User user = Accounts.getLoggedInUser();
    Comment comment = Comment.findById(Long.parseLong(commentId));
    Blog blog = comment.post.blog;
    Logger.info("Deleting comment: " + commentId);
    
    
    User author = comment.author;
    
    comment.delete();
    blog.numComments--;
    blog.save();
    author.save();
    
    index("" + blog.id);
  }
}
