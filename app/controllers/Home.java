package controllers;

import models.Blog;
import models.User;
import play.mvc.Controller;

public class Home extends Controller {
  
  public static void index() {
    User user = Accounts.getLoggedInUser();
    render(user);
  }
  
  public static void createBlog(Blog blog) {
    
    if (blog.title.length() > 0 && blog.description.length() > 0) {
    
      User user = Accounts.getLoggedInUser();
  
      blog.blogger = user;
      blog.url = blog.getTitle().replaceAll("\\s", "").toLowerCase();
      blog.save();
      
      user.blogs.add(blog);
      user.save();
  
      index();
    } else {
      index();
    }
  }

}
