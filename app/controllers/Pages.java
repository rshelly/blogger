package controllers;

import models.Blog;
import models.Page;
import models.Post;
import models.User;
import play.mvc.Controller;

public class Pages extends Controller {
  
  public static void listPages(String blogId) {
    User user = Accounts.getLoggedInUser();
    Blog blog = Blog.findById(Long.parseLong(blogId));
    render(user, blog);
  }
  
  public static void newPage(String blogId, Page page) {
    
    if (page.title.length() > 0 && page.content.length() > 0) {
      page.blog = Blog.findById(Long.parseLong(blogId));
      page.url = page.getTitle().replaceAll("\\s", "").toLowerCase();
      page.save();
    }
      listPages("" + blogId);
  }
  
  public static void deletePage(String pageId) {
    User user = Accounts.getLoggedInUser();
    Page page = Page.findById(Long.parseLong(pageId));
    Blog blog = page.blog;
    
    page.delete();
    user.save();
    listPages("" + blog.id);
  }
}
