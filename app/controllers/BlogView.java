package controllers;

import models.Blog;
import models.Comment;
import models.Page;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class BlogView extends Controller{
  
  public static void index(String blogUrl) {    
    Blog blog =  Blog.findByUrl(blogUrl);
    blog.views++;
    blog.save();
    
    if (blog.posts.size() > 0 || blog.pages.size() == 0) {
      render(blog);
    } else {
      Page page = blog.pages.get(0);
      page(blog.url, page.url);
    }
  }
  
  public static void page(String blogUrl, String pageUrl) {
    Blog blog = Blog.findByUrl(blogUrl);
    Page page = Page.findByUrl(pageUrl);
    render(blog, page);
  }
  
public static void newComment(String postId, String content) {
    
    User author = Accounts.getLoggedInUser();
    Post post = Post.findById(Long.parseLong(postId));
    
    Comment comment = new Comment(post, author, content);
    post.comments.add(comment);
    post.blog.numComments++;
    post.save();
    post.blog.save();
    
    author.comments.add(comment);
    author.save();
    
    index(post.blog.url);
  }
}
