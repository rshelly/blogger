package models;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class Page extends Model {
  
  @ManyToOne
  public Blog blog;
  
  public String title;
  @Lob
  public String content; 
  
  public String url;
  public String imageUrl = "";
  public String videoUrl = "";

  public Page(Blog blog, String title, String content) {
    
    this.blog = blog;
    this.title = title;
    this.content = content;    
  }
  
  public static Page findByUrl(String url) {
    return find("url", url).first();
  }
  
  public String getTitle() {
    return title;
  }
  
  public Blog getBlog() {
    return blog;
  }

}
