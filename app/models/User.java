package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity
public class User extends Model {

  public String firstName;
  public String lastName;
  public String about;
  public String avatarUrl;
  public String email;
  public String username;
  public String password;

  @OneToMany(mappedBy = "blogger", cascade=CascadeType.ALL)
  public List<Blog> blogs = new ArrayList<Blog>();
  
  @OneToMany (mappedBy="author", cascade=CascadeType.ALL)
  public List<Comment> comments = new ArrayList<Comment>();

  /**
   * Constructor for a user
   * 
   * @param firstName User's first name
   * @param lastName User's surname
   * @param about A Brief description of the user
   * @param avatarUrl The URL for the users's avatar image
   * @param email User's email address
   * @param username User's username
   * @param password User's password
   */
  public User(String firstName, String lastName, String about, String avatarUrl, String email, String username,
      String password) {

    this.firstName = firstName;
    this.lastName = lastName;
    this.about = about;
    if (avatarUrl.length() > 0) {
      this.avatarUrl = avatarUrl;
    } else {
      this.avatarUrl = " ";
    }
    this.email = email;
    this.username = username;
    this.password = password;
  }
  
  /** 
   * Find a user using their email
   * 
   * @param email Email address of the user to find
   * @return The user with the entered email address
   */
  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  /** 
   * Find a user using their username
   * 
   * @param username Username of the user to find
   * @return The user with the entered username
   */
  public static User findByUsername(String username)
  {
    return find("username", username).first();
  }
  
  public boolean verifyPassword(String password) {
    return (this.password.equals(password));
  }

}
