package models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity
public class Post extends Model{
  
  @ManyToOne
  public Blog blog;
  
  public String title;
  @Lob
  public String content;
  public String date;
  
  @OneToMany (mappedBy="post" , cascade=CascadeType.ALL)
  public List<Comment> comments = new ArrayList<Comment>();
  
  public String imageUrl = "";
  public String videoUrl = "";
  
  /**
   * Constructor for a post
   * 
   * @param blog The blog this post belongs to
   * @param title The title of the post
   * @param content The text of the post
   */
  public Post (Blog blog, String title, String content, String imageUrl, String videoUrl) {
    this.blog = blog;
    this.title = title;
    this.content = content;
    this.imageUrl = imageUrl;
    this.videoUrl = videoUrl.replaceAll("youtu.be", "www.youtube.com/embed");
    
    Date date = new Date();
    SimpleDateFormat  dateFormat =
        new SimpleDateFormat ("E',' MMM dd 'at' hh.mm aa");
    
    this.date = dateFormat.format(date);
  }
}
