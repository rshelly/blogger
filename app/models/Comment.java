package models;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class Comment extends Model{
  
  @ManyToOne
  public Post post;
  
  @ManyToOne
  public User author;
  public String content;
  public String date;
  
  /**
   * Constructor for a comment
   * 
   * @param post The post this comment is added to
   * @param author The author of the comment
   * @param content The comment text
   */
  public Comment (Post post, User author, String content) {
    this.post = post;
    this.author = author;
    this.content = content;

    Date date = new Date();
    SimpleDateFormat  dateFormat =
        new SimpleDateFormat ("E',' MMM dd 'at' hh.mm aa");
    
    this.date = dateFormat.format(date);
  }
  

}
