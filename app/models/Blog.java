package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.Logger;
import play.db.jpa.Model;

@Entity
public class Blog extends Model {

  @ManyToOne
  public User blogger;
  
  public String title;
  public String description;
  public String url;
  public String menuLayout;
  public String colour;
  public int numComments;
  public int views;
  
  @OneToMany (mappedBy = "blog", cascade=CascadeType.ALL)
  public List<Post> posts = new ArrayList<Post>();
  
  @OneToMany (mappedBy = "blog", cascade=CascadeType.ALL)
  public List<Page> pages = new ArrayList<Page>();
  

  /**
   * Constructor for a blog
   * 
   * @param user The author of the blog
   * @param title The title of the blog
   * @param description A brief description of the blog
   */
  public Blog(User blogger, String title, String description) {
    this.blogger = blogger;
    this.title = title;
    this.description = description;
    menuLayout = "fluid";
    numComments = 0;
    views = 0;
  }
  
  public static Blog findByUrl(String url) {
    return find("url", url).first();
  }
  
  public String getTitle() {
    return title;
  }
}
